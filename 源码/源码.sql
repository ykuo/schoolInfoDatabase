/*建立学生表*/
create table 学生表(
    学号 char(8) primary key ,姓名 char(8),
    性别 char(2) check ( 性别 in ('男','女')),
    所在系 char(20) , 专业 char(20),班号 char(6)
)
/*建立课程表*/
create table 课程表(
    课程号 char(8) primary key , 课程名 varchar(30) not null ,
    学分 tinyint check ( 学分 between 1 and 12),
    课程性质 char(4) check ( 课程性质 in ('选修','必修') ),
    考试性质 char(4) check ( 考试性质 in ('考试','考查') ),
    授课时数 tinyint check ( 授课时数 <= 68),
    实践时数 tinyint check ( 实践时数 <= 68),
    平时成绩比例 numeric(1,1)
)
/*建立教师表*/
create table 教师表(
    教师号 char(10) primary key ,
    教师名 char(8) not null ,
    性别 char(2) check ( 性别 in ('男','女')),
    职称 char(6) check ( 职称 in ('助教','讲师','副教授','教授')),
    学历 char(6) check ( 学历 in ('本科','硕士','博士','博士后')),
    出生日期 datetime ,所在部门 varchar(30))
/*建立选课表*/
create table 选课表(
    学号 char(8) not null, 课程号 char(10) not null,
    选课学年 char(4), 选课学期 char(1) check(选课学期 like '[12]'),
    primary key (学号,课程号,选课学年,选课学期),
    foreign key (学号) references 学生表 (学号),
    foreign key (课程号) references 课程表 (课程号)
)
/*建立成绩表*/
create table 成绩表(
    学号 char(8) not null, 课程号 char(10) not null ,
    考试次数 tinyint check ( 考试次数 between 1 and 3),
    平时成绩 tinyint, 考试成绩 tinyint, 总评成绩 tinyint,
    primary key (学号,课程号),
    foreign key (学号) references 学生表 (学号),
    foreign key (课程号) references 课程表 (课程号)
)
/*建立授课表*/
create table 授课表(
    课程号 char(10) not null , 教师号 char(10) not null ,
    学年 char(4), 学期 tinyint,
    主讲日期 tinyint, 辅导时数 tinyint, 带实验时数 tinyint,
    primary key (课程号,教师号,学年,学期),
    foreign key (课程号) references 课程表 (课程号),
    foreign key (教师号) references 教师表 (教师号)
)
/*建立视图*/
/*学生选课情况（学号，姓名，班号，课程名）
/*视图v1*/
create view V1 (学号,姓名,班号,课程号)
    as select SC.学号,姓名,班号,课程名
        from 学生表 S join 选课表 SC on SC.学号 = S.学号
                     join 课程表 C on SC.课程号= C.课程号
/*学生累计修课总学分情况（学号，姓名，班号，总学分）*/
/*视图v2*/
create view V2(学号,总学分)
    as select S.学号 , sum(学分)
        from 学生表 S join 成绩表 G on S.学号=G.学号
                    join 课程表 C on G.课程号 = C.课程号
        where 总评成绩 >= 60
        group by S.学号
/*视图v3*/
create view V3(学号,补考门次)
as
    select S.学号,COUNT(*)
from 学生表 S join 成绩表 G on S.学号 = G.学号
where 考试次数 >1
group by S.学号




/*实现安全控制要求*/
/*建立数据库用户*/
CREATE USER 'SQL_SERVER'@'localhost' IDENTIFIED BY 'Abc123'
/*建立角色*/
/*先创建角色，然后赋予语句权限（如果有），在赋予对象权限，最后添加成员*/
/*系统管理员*/
/*因为每个数据库管理系统在安装好之后都有自己默认的系统管理员"sa"，所以不能建立*/
/*教务部门*/
/*
create role 教务
grant create on test.* to 教务
grant create view on test.* to 教务
grant create routine on test.* to 教务
grant trigger on test.* to 教务
grant select,insert,delete,update on 课程表 to 教务
grant select,insert,delete,update on 学生表 to 教务
grant select,insert,delete,update on 授课表 to 教务
grant select,insert,delete,update on 成绩表 to 教务
exec sp_rolemember'教务'，'教务成员'
*/
create role 教务
grant create table to 教务
grant create view to 教务
grant create procesuce to 教务
grant create trigger to 教务
grant create function to 教务
grant select,insert,delete,update on 课程表 to 教务
grant select,insert,delete,update on 学生表 to 教务
grant select,insert,delete,update on 授课表 to 教务
grant select,insert,delete,update on 成绩表 to 教务
execute sp_rolemember '教务','教务成员'
/*人事部门*/
create role 人事
grant create table to 人事
grant create view to 人事
grant create procedure to 人事
grant create trigger to 人事
grant create function to 人事
grant select,insert,delete,update on 教师表 to 人事
execute sp_rolemember '人事','人事成员'
/*各个系*/
create role 系
grant create table to 人事
grant create view to 人事
grant create procedure to 人事
grant create trigger to 人事
grant create function to 人事
/*普通用户*/
create role 用户
grant select on 学生表 to 用户
grant select on 课程表 to 用户
grant select on 教师表 to 用户
grant select on 选课表 to 用户
grant select on 成绩表 to 用户
grant select on 授课表 to 用户
execute sp_rolemember '用户','普通成员'




/*数据的查询功能*/
/*根据系、专业、班等信息查询学生的基本信息*/
select * from 学生表 where 所在系='海洋与环境学院'
select * from 学生表 where 专业='环境工程'
select * from 学生表 where 所在系='180542'
/*根据学期查询课程的基本信息*/
select * from 课程表 where 开课学期 = 1
/*根据部门查询教师的基本信息*/
select * from 教师表 where 所在部门='学工部'
/*根据班号查询学生当前学期和学年的选课情况*/
select 姓名,学号,课程号,课程名,选课学期
from 学生表 S join 选课表 SC on S.学号 = SC.学号
join 课程表 C on C.课程号 = SC.课程号
where 班号 ='180542'
/*根据班号查询学生在当前学期和学年的考试情况*/
select 姓名,学号,课程号,考试次数,平时成绩,考试次数,考试成绩,总评成绩,选课学期
from 学生表 S join 选课表 SC on S.学号 = SC.学号
join 成绩表 G on G.课程号=SC.课程号
where 班号='180542'
/*根据课程查询当前学期和学年学生的选课及考试情况*/
select 姓名,学号,选课学期,课程号,课程名,考试次数,考试成绩,平时成绩,总评成绩
from 课程表 C join 选课表 SC on C.课程号=SC.课程号
join 成绩表 G on SC.课程号=G.课程号
join 学生表 S on G.学号=S.学号
where 课程号='01'
/*根据部门、职称查询教师的授课情况*/
select 教师号,教师名,课程号,课程名,学年,学期,主讲时数,辅导时数,带实验时数
from 教师表 T join 授课表 TC on T.教师号:TC.教师号
join 课程表 C on TC.课程号=C.课程号
where 部门='学工部'/*(职称='教授')
/*统计每个部门的各种职称的教师人数*/
select 部门,职称,COUNT(职称) as 职称人数
from 教师表 group by 部门
/*统计当前学期和学年每门课程的选课人数*/
select 课程号,课程名,选课学期,COUNT(*) as 选课人数
from 选课表 group by 课程号
/*按班统计当前学期和学年每个学生的考试平均成绩*/
select 班号,学号,姓名,选课学期,avg(总评成绩) as 平均成绩
from 学生表 S join 选课表 SC on S.学号=SC.学号
join 成绩表 G on SC.课程号=G.课程号
group by 班号
/*按班统计每个班考试平均成绩最高的前三名学生*/
select top 3 with ties 班号,学号,姓名,avg(总评成绩) as 平均成绩
from 学生表 S join 成绩表 G on S.学号=G.学号
group by 班号 order by avg(总评成绩) desc





/*数据的插入，删除和修改功能*/
/*在学生表中插入数据的存储过程*/
create procedure insert_student(
    @学号 char(8),@姓名 char(6),@性别 char(2),@所在系 char(20),@专业 char(20),@班号 char(6)
)
as insert into 学生表(学号,姓名,性别,所在系,专业,班号)
values(@学号,@姓名,@性别,@所在系,@专业,@班号)
/*在学生表中删除数据的存储过程*/
create procedure delete_student(@学号 char(2))
as delete 学生表 where 学号=@学号
/*学生表中修改数据的存储过程*/
create procedure update_student(
    @_学号 char(8),@学号 char(8)=null,@姓名 char(6)=null,@性别 char(2)=null,@所在系 char(20)=null,@专业 char(20)=null,@班号 char(6)=null
)
as
if(@学号 !=null) update 学生表 set 学号=@学号 where 学号=@_学号
if(@姓名 !=null) update 学生表 set 姓名=@姓名 where 学号=@_学号
if(@性别 !=null) update 学生表 set 性别=@性别 where 学号=@_学号
if(@所在系 !=null) update 学生表 set 所在系=@所在系 where 学号=@_学号
if(@专业 !=null) update 学生表 set 专业=@专业 where 学号=@_学号
if(@班号 !=null) update 学生表 set 班号=@班号 where 学号=@_学号