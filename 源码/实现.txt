CREATE TRIGGER tri_UpdateTimes ON 成绩表 FOR insert
	As
		declare @cs int
			IF (SELECT COUNT(*) FROM 成绩表 --如果是第1次考试成绩
				WHERE 学号 IN (SELECT 学号 FROM INSERTED) AND 课程号 IN (SELECT 课程号 FROM INSERTED)) = 1
				UPDATE 成绩表 SET 考试次数 = 1
				WHERE 学号 IN (SELECT 学号 FROM INSERTED) AND 课程号 IN (SELECT 课程号 FROM INSERTED)
			ELSE --如果不是第1次考试成绩
			BEGIN
				SELECT @cs = MAX(考试次数) FROM 成绩表 --取最大考试次数
					WHERE 学号 IN (SELECT 学号 FROM INSERTED) AND 课程号 IN (SELECT 课程号 FROM INSERTED) --修改考试次数
				IF @cs < 3
					UPDATE 成绩表 SET 考试次数 = @cs + 1
						WHERE 学号 IN (SELECT 学号 FROM INSERTED) AND 课程号 IN (SELECT 课程号 FROM INSERTED)
			END

CREATE TRIGGER tri_FinalGrade ON 成绩表 FOR insert
	AS
		UPDATE 成绩表 SET 总评成绩 = CASE
			WHEN 考试次数 = 1 THEN 平时成绩 * 平时成绩比例 + 考试成绩 * (1 - 平时成绩比例)
		ELSE 考试成绩
		END
		FROM 成绩表 a JOIN 课程表 b ON a.课程号 = b.课程号
			WHERE 学号 IN (SELECT 学号 FROM INSERTED) AND a.课程号 IN (SELECT 课程号 FROM INSERTED)
